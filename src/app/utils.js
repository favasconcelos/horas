const DEFAULT_TIME = '--:--';
const REGEX = /\d{2}:\d{2}/;
const MAX_SEC = 24 * 60 * 60;

export function toSec(h, m) {
  h = parseInt(h);
  m = parseInt(m);
  return (h * 60 + m) * 60;
}

export function toTime(sec) {
  if (!sec || sec <= 0 || sec > MAX_SEC) {
    return DEFAULT_TIME;
  }
  const date = new Date(null);
  date.setSeconds(sec);
  return date.toISOString().substr(11, 5);
}

export function calculateDiff(start, end) {
  if (!start || !end || !REGEX.test(start) || !REGEX.test(end)) {
    return 0;
  }
  const [h1, m1] = end.split(':');
  const [h2, m2] = start.split(':');
  const s1 = toSec(h1, m1);
  const s2 = toSec(h2, m2);
  if (s1 < s2) {
    return 0;
  }
  if (s1 > MAX_SEC || s2 > MAX_SEC) {
    return 0;
  }
  return s1 - s2;
}

export function createUUID() {
  const dt = new Date().getTime();
  return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, c => {
    const r = (dt + Math.random() * 16) % 16 | 0;
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
}
