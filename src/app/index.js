import React, { useState } from 'react';
import { PlusSquare, RefreshCcw } from 'react-feather';
// local
import './App.scss';
import imgLogo from '../_assets/logo.webp';
import Field from './field';
import { toTime, calculateDiff, createUUID } from './utils';

function createItem() {
  return { id: createUUID(), start: '', end: '' };
}

const defaultFields = () => [createItem(), createItem(), createItem()];

export default function App() {
  const [fields, setFields] = useState(defaultFields());

  function handleUpdate(object, attr, value) {
    const map = fields.map(field => {
      if (field.id === object.id) {
        field[attr] = value;
      }
      return field;
    });
    setFields(map);
  }

  function handleRemove(id) {
    const filtered = fields.filter(item => item.id !== id);
    setFields(filtered);
  }

  function handleAdd() {
    const newItem = createItem();
    setFields([...fields, newItem]);
  }

  function handleRefresh() {
    setFields(defaultFields());
  }

  const totalDiff = fields.reduce((acc, field) => acc + calculateDiff(field.start, field.end), 0);
  const totalTime = toTime(totalDiff);
  return (
    <div id="app">
      <div className="header">
        <div className="title">
          <img src={imgLogo} alt="Logo" />
          <h1>Horas</h1>
        </div>
        <div className="actions">
          <span title="Addicionar linha">
            <PlusSquare onClick={handleAdd} />
          </span>
          <span title="Recarregar">
            <RefreshCcw onClick={handleRefresh} />
          </span>
        </div>
      </div>
      <div id="fields">
        {fields.map((field, i) => (
          <Field key={`field-${i}`} onUpdate={handleUpdate} onRemove={handleRemove} field={field} />
        ))}
      </div>
      <div className="footer">
        <div className="result">{totalTime}</div>
      </div>
    </div>
  );
}
