import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import { Trash } from 'react-feather';
// local
import { toTime, calculateDiff } from '../utils';

const HOUR_MASK = [/\d/, /\d/, ':', /\d/, /\d/];

export default function Field({ field, onUpdate, onRemove }) {
  function handleStart({ target }) {
    onUpdate(field, 'start', target.value);
  }

  function handleEnd({ target }) {
    onUpdate(field, 'end', target.value);
  }

  function handleRemove() {
    onRemove(field.id);
  }

  const diff = calculateDiff(field.start, field.end);
  const time = toTime(diff);

  const fieldStartID = `field-start-${field.id}`;
  const fieldEndID = `field-end-${field.id}`;
  return (
    <div className="field">
      <div className="field-group">
        <label htmlFor={fieldStartID}>Começo</label>
        <MaskedInput id={fieldStartID} mask={HOUR_MASK} placeholder="00:00" onChange={handleStart} value={field.start} />
      </div>
      <div className="field-group">
        <label htmlFor={fieldEndID}>Fim</label>
        <MaskedInput id={fieldEndID} mask={HOUR_MASK} placeholder="00:00" onChange={handleEnd} value={field.end} />
      </div>
      <div className="diff">{time}</div>
      <span title="Remover linha">
        <Trash onClick={handleRemove} />
      </span>
    </div>
  );
}

Field.propTypes = {
  field: PropTypes.objectOf(PropTypes.string).isRequired,
  onUpdate: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};
